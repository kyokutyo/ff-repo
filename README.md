これは git merge の --no-ff を確認するためのリポジトリです

# コミットグラフの比較

## --ff

- [kyokutyo / ff-repo / Commits — Bitbucket](https://bitbucket.org/kyokutyo/ff-repo/commits/all)

## --no-ff

- [kyokutyo / no-ff-repo / Commits — Bitbucket](https://bitbucket.org/kyokutyo/no-ff-repo/commits/all)

# 実行したコマンド

## ff-repo

    git init
    # master ブランチで Initial commit
    echo '- fuga' > test.md
    git add test.md
    git commit -m'Initial commit'
    # topic ブランチで編集&編集
    git checkout -b topic
    echo '- hoge' >> test.md
    git add test.md
    git commit -m'Add hoge(@topic)'
    echo '- dodo' >> test.md
    git add test.md
    git commit -m'Add dodo(@topic)'
    # master にマージする(fast-forward)
    git checkout master
    git merge --ff topic
    # README.md をコピーする
    cp ../README.md .
    git add README.md
    git commit -m'Add README.md(@master)'
    # リモートへ push
    git remote add origin git@bitbucket.org:kyokutyo/ff-repo.git
    git push -u origin --all

## no-ff-repo

    git init
    # master ブランチで Initial commit
    echo '- fuga' > test.md
    git add test.md
    git commit -m'Initial commit'
    # topic ブランチで編集&編集
    git checkout -b topic
    echo '- hoge' >> test.md
    git add test.md
    git commit -m'Add hoge(@topic)'
    echo '- dodo' >> test.md
    git add test.md
    git commit -m'Add dodo(@topic)'
    # master にマージする(non fast-forward)
    git checkout master
    git merge --no-ff topic
    # README.md をコピーする
    cp ../README.md .
    git add README.md
    git commit -m'Add README.md(@master)'
    # リモートへ push
    git remote add origin git@bitbucket.org:kyokutyo/no-ff-repo.git
    git push -u origin --all
